<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: payer
 * Date: 10/14/16
 * Time: 7:55 PM
 */
class Map extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Map_m');
        $this->load->helper('common');
    }

    public function index(){
        $data['route_detail'] = $this->Map_m->getRouteLatLonByRouteId(1);
        $this->load->view('map/gmap',$data);
    }

    public function track($route_id=1){
        $data['route_id'] = $route_id;
        $data['route_detail'] = $this->Map_m->getRouteLatLonByRouteId($route_id);
        $this->load->view('map/track',$data);
    }

    public function trackRouteByCurrentPosition($route_id=1){
//        $data['route_id'] = $route_id;
//        $data['route_detail'] = $this->Map_m->getRouteLatLonByRouteId($route_id);
//        $this->load->view('map/track_route_by_current_position',$data);
        //change requirement so call trackRoute function
        $this->trackRoute($route_id);
    }

    public function trackRoute($route_id=1){
        $data['route_id'] = $route_id;
        $route_info = $this->Map_m->getRouteInitialPoint($route_id);
        $data['lat'] = $route_info[0]['lat'];
        $data['lon'] = $route_info[0]['lon'];
        $data['route_detail'] = $this->Map_m->getRouteLatLonByRouteId($route_id);
        $this->load->view('map/track_route',$data);
    }



    public function test(){
        $data['route_detail'] = $this->Map_m->getRouteLatLonByRouteId(1);
        $this->load->view('map/test_gmap',$data);
    }


    public function getRouteData(){
        $limit = $this->input->post('limit');
        $route_id = $this->input->post('route_id');
        $route_detail = $this->Map_m->getRouteLatestData($route_id,$limit);
        echo json_encode($route_detail,JSON_NUMERIC_CHECK);
    }

    public function getUpdatedRouteData(){
        $route_id = $this->input->post('route_id');
        $route_detail = $this->Map_m->getRouteLastSyncData($route_id);
        echo json_encode($route_detail,JSON_NUMERIC_CHECK);
    }

    public function getApproximateRouteId($lat,$lon,$distance=100){
        $result = $this->Map_m->getApproximateRouteId($lat,$lon,$distance);
        echo json_encode($result);
    }

    public function getAllRoutes(){
        $result = $this->Map_m->getAllRoutes();
        wrapJson(true, "", $result);
    }

}
