<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_m');
		$this->load->helper('common');
		$this->check_api_key();
	}

	public function login(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = md5($password);
		$data = $this->Auth_m->user_check($username,$password);
		
		if(empty($data)){
			$result = array('success'=>false,'message'=>'Invalid username or password','data'=>array());
		}else{
			if($data[0]['is_active']==1){
				$message = "Welcome ".$data[0]['first_name']."";
				$result = array('success'=>true,'message'=>$message,'data'=>$data);
			}else{
				$result = array('success'=>false,'message'=>'inactive user','data'=>array());
			}

		}
		echo json_encode($result);
	}



	public function change_password(){

	}

	public function checkValidation(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$message = "";

		if(strlen($first_name)<3){
			$message .= "First Name must be greater then 2 character";
		}else if(strlen($last_name)<3){
			$message .= "Last Name must be greater then 2 character";
		}else if(strlen($username)<6){
			$message .= "Username must be greater then 5 character";
		}else if(strlen($password)<6){
			$message .= "Password must be greater then 5 character";
		}else if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$message .= "Invalid Email Address";
		}else if(!preg_match("/^[0][0-9]*$/", $phone)) {
			$message .= "Invalid Phone number";
		}else if(strlen($phone) !=11){
			$message .= "Phone No. must be 11 Digit";
		}



		if($message !=""){
			wrapJson(false, $message, array(array()));
		exit();
		}
	}

	public function checkEmptyValue(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$message = "";

		if($first_name==''){
			$message .= "Please provide first name";
		}else if($last_name==''){
			$message .= "Please provide last name";
		}else if($username==''){
			$message .= "Please provide username";
		}else if($password==''){
			$message .= "Please provide password";
		}else if($email==''){
			$message .= "Please provide email";
		}else if($phone==''){
			$message .= "Please provide phone";
		}



		if($message !=""){
			wrapJson(false, $message, array(array()));
		exit();
		}
	}
	public function isUserExist(){

		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$message = "";

		$is_username_exist = $this->Auth_m->isUserNameExist($username);
		$is_email_exist = $this->Auth_m->isEmailExist($email);
		$is_phone_exist = $this->Auth_m->isPhoneExist($phone);

		if($is_username_exist){
			$message .="This usename is already exist";
		}else if($is_email_exist){
			$message .="This email is already exist";
		}else if($is_phone_exist){
			$message .="This Phone is already exist";
		}


		if($message !=""){
			wrapJson(false, $message, array(array()));
		exit();
		}
	}

	public function create_user(){
		$this->checkEmptyValue();
		$this->checkValidation();
		$this->isUserExist();
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		$data = array(
			'username'=>$username,
			'password'=>md5($password),
			'first_name'=>$first_name,
			'last_name'=>$last_name,
			'email'=>$email,
			'phone'=>$phone,
			'is_active'=>1
		);

		$result = $this->Auth_m->insertData('tbld_user',$data);
		$data['id']=$result;
		
		if($result){
			wrapJson(true, 'Successfully Register', array($data));
		}else{
			wrapJson(false, 'Error occured', array($data));
		}


	}

	public function edit_user(){

	}

	public function activate(){

	}

	public function deactivate(){

	}

	public function check_api_key(){
		if(isset($_REQUEST['api_key'])){
			$api_key = $_REQUEST['api_key'];
			if($api_key !=$this->config->item('api_key')){
				echo json_encode(array('error'=>'Invalid API Key'));
				exit();
			}
		}else{
			echo json_encode(array('error'=>'Please, provide API key'));
			exit();
		}


	}
}
