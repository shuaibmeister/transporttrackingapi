<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {

    private $table_raw_location = "tbld_tracking_raw_data";
    private $table_waiting_data = "tbld_waiting_data";
    
    public function __construct()
    {
            parent::__construct();
            $this->load->model('Location_m');
            $this->load->model('Map_m');
            $this->load->helper('common');
    }

//    public function updateTrackingLocation(){
//        $data = $this->getTrackingDataFromPost();
//        $data['route_id'] = $this->input->post('route_id');
//        $this->insertIntoDb($this->table_raw_location, $data);
//    }

    public function test(){
        $data = array(
            'user_id'=>3,
            'lat'=>23.877560000000003,
            'lon'=>90.26590166666668,
            'accuracy'=>1,
            'timestamp'=> date("Y-m-d H:i:s")
        );

        $this->updateTrackingLocationDecision($data);
    }

    //if updateTrackingLocation is working fine then replace existing function with the following comment out function
    public function updateTrackingLocation(){
        $data = $this->getTrackingDataFromPost();
        $result =$this->updateTrackingLocationDecision($data);
    }

    public function updateTrackingLocationDecision($data){
        $lat = $data['lat'];
        $lon = $data['lon'];
        $result = $this->getApproximateRouteId($lat,$lon,100);
        if(!empty($result)){
            $data['sequence'] = $result[0]['sequence'];
            $data['route_id'] = $result[0]['route_id'];
            $this->insertData($this->table_raw_location, $data);
        }

    }

    public function getApproximateRouteId($lat,$lon,$distance=100){
        $result = $this->Map_m->getApproximateRouteId($lat,$lon,$distance);
        return $result;
    }
    
    public function updateMyLocation(){
        $data = $this->getTrackingDataFromPost();
        $this->insertIntoDb($this->table_waiting_data, $data);
    }
    
    private function getTrackingDataFromPost(){
        $user_id = $this->input->post('user_id');
        $lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
        $accuracy = $this->input->post('accuracy');

        $data = array(
                'user_id'=>$user_id,
                'lat'=>$lat,
                'lon'=>$lon,
                'accuracy'=>$accuracy,
                'timestamp'=> date("Y-m-d H:i:s")
        );
        return $data;
    }

    private function insertData($table_name, $data){
        $result = $this->Location_m->insertData($table_name, $data);

        if($result){
            wrapJson(true, 'Success', $result);
        } else {
            wrapJson(true, 'error occured', $result);
        }
    }
    
    private function insertIntoDb($table_name, $data){
        $result = $this->Location_m->insertData($table_name, $data);

        if($result){
            $message='Successfully Register';
            wrapJson(true, $message, $result);
        } else {
            $message = 'Error occured';
            wrapJson(true, $message, $result);
        }
    }
}
