<!DOCTYPE html>
<html>
<head>
    <title>Route Track</title>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
        }

    </style>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
</head>
<body>

<div id="map"></div>

<script>


    var map;
    var markers = [];
    var initial_position = {lat: <?php echo $lat;?>, lng: <?php echo $lon;?>};

    var route_id = <?php echo $route_id;?>;
    var route_path = [
        <?php foreach ($route_detail as $v) {
            echo '{lat: '.$v['lat'].', lng: '.$v['lng'].'},';
        }?>
    ];

    var Path = [];
        Path.push(initial_position);


    $(document).ready(function(){
        makeUpdate();
        setInterval("makeUpdate()", 5000);
    });


    function makeUpdate(){

        $.ajax({
            type: "POST",
            async:false,
            url: "<?php echo base_url(); ?>map/getUpdatedRouteData/",
            data: {route_id:route_id},
            dataType: "json",
            success: function(data) {
                if(data.length !=0){
                    initial_position = data[0];
                    Path = data;
                }
            }
        });
        google.maps.event.trigger(map, 'click');
    }


    function initMap() {


        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: initial_position,
            mapTypeId: 'terrain'
        });


        map.addListener('click', function(event) {
            addMarker(initial_position);
            updatePolyline();
        });

        var Path_info = new google.maps.Polyline({
            path: route_path,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        Path_info.setMap(map);



        var route_info = new google.maps.Polyline({
            path: route_path,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        route_info.setMap(map);




        addMarker(initial_position);

    }


    function updatePolyline(){

        var route_info = new google.maps.Polyline({
            path: Path,
            geodesic: true,
            strokeColor: '#00FF00',
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        route_info.setMap(map);
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        deleteMarkers();
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            //icon: 'http://localhost/Dropbox/api/assets/school_bus.png'
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'

        });
        markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }


    function deleteMarkers() {
        setMapOnAll(null);
        markers = [];
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbCYFj1qTC4Ds1erRy6THxuvhldMZ8I2w&callback=initMap"> </script>
</body>
</html>