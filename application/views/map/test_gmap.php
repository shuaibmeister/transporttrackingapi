<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Polylines</title>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
</head>
<body>
<div id="map"></div>
<script>

    var limit = 0;

    $(document).ready(function(){
       setInterval("makeUpdate()", 1000);
    });

    var route_path = [
        <?php foreach ($route_detail as $v) {
            echo '{lat: '.$v['lat'].', lng: '.$v['lng'].'},';
        }?>
    ];

    var Path = [];


    var map
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: {lat: 23.777280, lng: 90.380369},
            mapTypeId: 'terrain'
        });

        map.addListener('click', function(e) {
            updatePolyline();
        });

        var Path_info = new google.maps.Polyline({
            path: route_path,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        Path_info.setMap(map);
    }

    function updatePolyline(){

        var Path_info = new google.maps.Polyline({
            path: Path,
            geodesic: true,
            strokeColor: '#00FF00',
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        Path_info.setMap(map);
    }

    function makeUpdate(){
        limit++;
        $.ajax({
            type: "POST",
            async:false,
            url: "<?php echo base_url(); ?>map/getRouteData/",
            data: {route_id:1,limit:limit},
            dataType: "json",
            success: function(data) {
                Path.push(data[0]);
            }
        });
        google.maps.event.trigger(map, 'click');
    }






</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbCYFj1qTC4Ds1erRy6THxuvhldMZ8I2w&callback=initMap"> </script>

</body>
</html>