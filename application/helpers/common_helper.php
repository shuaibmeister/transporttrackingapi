<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: shuaib
 * Date: 10/17/16
 * Time: 11:44 PM
 */
if ( ! function_exists('wrapJson')) {

    function wrapJson($success, $message, $json)
    {
        echo json_encode(array('success' => $success, 'message' => $message, 'data' => $json));
    }
}
?>
