<?php

/**
 * Created by PhpStorm.
 * User: payer
 * Date: 10/8/16
 * Time: 3:42 PM
 */
class Location_m extends CI_Model
{
    /*--------------------------------------------------------------------------
     * Insert data to given table
     * @param_1 = table name
     * @param_2 = table data
     * @return last insert id
     *-------------------------------------------------------------------------*/
    public function insertData($tbl, $data)
    {

        if(!$this->db->insert($tbl, $data)){

            return $this->db->error(); // Has keys 'code' and 'message'

        }else{
            return $this->db->insert_id();
        }
    }

}