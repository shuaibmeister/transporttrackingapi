<?php

/**
 * Created by PhpStorm.
 * User: payer
 * Date: 10/8/16
 * Time: 3:42 PM
 */
class Map_m extends CI_Model
{
    public function getAllRoutes()
    {
        $sql = "select * from tbld_route";
        return $this->db->query($sql)->result_array();
    }

    public function getRouteLatLonByRouteId($route_id)
    {
        $sql = "select lat,lon as lng from tbld_traking_master_data where route_id=$route_id order by sequence asc";
        return $this->db->query($sql)->result_array();
    }

    public function getRouteLatestData($route_id, $limit)
    {
        $sql = "select lat,lon as lng from tbld_traking_master_data where route_id=$route_id order by sequence asc limit $limit ,1";
        return $this->db->query($sql)->result_array();
    }

    public function getRouteLastSyncData($route_id)
    {
        $sql = "SELECT lat,lon as lng
                FROM `tbld_traking_master_data`
                WHERE sequence <= (SELECT max(sequence)
                                   FROM `tbld_tracking_raw_data` AS t1
                                   WHERE t1.route_id = $route_id AND date(timestamp) = curdate()) AND route_id = $route_id
                ORDER BY sequence DESC

";
        return $this->db->query($sql)->result_array();
    }

    public function getRouteInitialPoint($route_id)
    {
        $sql = "SELECT * FROM `tbld_traking_master_data` where route_id=$route_id and sequence=1";
        return $this->db->query($sql)->result_array();
    }

    public function getApproximateRouteId($lat, $lon, $distance)
    {
        $sql = "SELECT
                    *,
                    (
                        6371000 *
                        acos(
                            cos( radians($lat) ) *
                            cos( radians( `lat` ) ) *
                            cos(radians( `lon` ) - radians($lon))
                            +
                            sin(radians($lat)) *
                            sin(radians(`lat`))
                        )
                    ) as `distance`
                FROM
                    `tbld_traking_master_data`
                HAVING
                    `distance` < $distance
                ORDER BY
                    `distance`
                LIMIT
                    1";
        return $this->db->query($sql)->result_array();
    }


    /*--------------------------------------------------------------------------
     * Insert data to given table
     * @param_1 = table name
     * @param_2 = table data
     * @return last insert id
     *-------------------------------------------------------------------------*/
    public function insertData($tbl, $data)
    {

        if (!$this->db->insert($tbl, $data)) {

            return $this->db->error(); // Has keys 'code' and 'message'

        } else {
            return $this->db->insert_id();
        }
    }

}